{!! HTML::style(asset('assets/stylesheet/ticket.css')) !!}
<style>
    .ticket {
        border: 1px solid {{$event->ticket_border_color}};
        background: {{$event->ticket_bg_color}} ;
        color: {{$event->ticket_sub_text_color}};
        border-left-color: {{$event->ticket_border_color}} ;
    }
    .ticket h4 {color: {{$event->ticket_text_color}};}
    .ticket .logo {
        border-left: 1px solid {{$event->ticket_border_color}};
        border-bottom: 1px solid {{$event->ticket_border_color}};

    }
</style>
<div class="ticket">
    <div class="logo">
        {!! HTML::image(asset($image_path)) !!}
    </div>

    <div class="event_details">
        <h4>Evento</h4>Evento de Prueba<h4>Organizador</h4>Organizador de Prueba<h4>Lugar de Encuentro</h4>Calle 123, Rosario, Santa Fe<h4>Fecha Inicio</h4>
        Sabado 17, 23:30 hs
        <h4>Fecha Fin</h4>
        Domingo 18 6:00 hs
    </div>

    <div class="attendee_details">
        <h4>Nombre</h4>Martin Perez<h4>Tipo de Entrada</h4>
        Entrada General
        <h4>N° Ref.</h4>
        #YLY9U73
        <h4>N° Ref. Asistente</h4>
        #YLY9U73-1
        <h4>Precio</h4>
        $XX.XX
    </div>

    <div class="barcode">
        {!! DNS2D::getBarcodeSVG('hello', "QRCODE", 6, 6) !!}
    </div>
    @if($event->is_1d_barcode_enabled)
        <div class="barcode_vertical">
            {!! DNS1D::getBarcodeSVG(12211221, "C39+", 1, 50) !!}
        </div>
    @endif
</div>
