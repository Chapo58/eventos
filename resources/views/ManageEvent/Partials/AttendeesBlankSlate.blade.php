@extends('Shared.Layouts.BlankSlate')


@section('blankslate-icon-class')
    ico-users
@stop

@section('blankslate-title')
    Aún no hay asistentes
@stop

@section('blankslate-text')
    Los asistentes aparecerán aquí una vez que hayan reservado una entrada para el evento, o, podés invitarlos manualmente.
@stop

@section('blankslate-body')
<button data-invoke="modal" data-modal-id='InviteAttendee' data-href="{{route('showInviteAttendee', array('event_id'=>$event->id))}}" href='javascript:void(0);'  class=' btn btn-success mt5 btn-lg' type="button" >
    <i class="ico-user-plus"></i>
    Enviar Invitación
</button>
@stop
