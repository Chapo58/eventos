@extends('Shared.Layouts.BlankSlate')


@section('blankslate-icon-class')
    ico-cart
@stop

@section('blankslate-title')
    No hay pedidos todavía
@stop

@section('blankslate-text')
    Los nuevos pedidos aparecerán aquí.
@stop

@section('blankslate-body')

@stop
