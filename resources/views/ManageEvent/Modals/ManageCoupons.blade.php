<div role="dialog"  class="modal fade " style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">
                    <i class="ico-question"></i>
                    Cupones</h3>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#QuestionsAccordion" href="#collapse{{$i}}" class="collapsed">
                                <span class="arrow mr5"></span> ¿Qué edad tienes?
                            </a>
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="required">
                                        Pregunta
                                    </label>
                                    <input placeholder="What is your name?" class="form-control" type="text" name="title" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>
                                        Tipo de Pregunta
                                    </label>
                                    <select class="form-control" name="question_type_id">
                                        <option value="1">
                                            Cuadro de Texto
                                        </option>
                                        <option value="2">
                                            Lista Desplegable
                                        </option>
                                        <option value="3">
                                            Checkbox (Verdadero/Falso)
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>
                                        Instrucciones
                                    </label>
                                    <input class="form-control" type="text" name="instructions" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>
                                        Optiones de Preguntas
                                    </label>
                                    <input placeholder="ej: oprción 1, oprción 2, oprción 3" class="form-control" type="text" name="options" />
                                    <div class="help-block">
                                        Por favor utilice comas para separar las opciones.
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="checkbox custom-checkbox">
                                        <input type="checkbox" id="requiredq" value="option1">
                                        <label for="requiredq">&nbsp; Pregunta Obligatoria</label>
                                    </span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="panel-footer">
                        <div class="form-group no-border">
                            <div class="col-sm-12">
                                <button  class="btn btn-danger deleteThis float-right">Eliminar Pregunta</button>
                                <button type="submit" class="btn btn-success float-right">Guardar Preguntas</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- /end modal body-->
            <div class="modal-footer">
                <a href="" class="btn btn-danger" data-dismiss="modal">
                    Cancelar
                </a>
                <a href="" class="btn btn-success">
                    Crear Pregunta
                </a>
                <button data-modal-id="CreateTicket" href="javascript:void(0);"  data-href="{{route('showCreateTicket', array('event_id'=>$event->id))}}" class="loadModal btn btn-success" type="button" ><i class="ico-ticket"></i> Create Ticket</button>
            </div>
        </div><!-- /end modal content-->
    </div>
</div>
