@extends('Shared.Layouts.BlankSlate')


@section('blankslate-icon-class')
    ico-search
@stop

@section('blankslate-title')
    Búsqueda sin resultados
@stop

@section('blankslate-text')
    No se encontró nada que coincidiera con el término '{{isset($search['q']) ? $search['q'] : $q}}'
@stop

@section('blankslate-body')

@stop
