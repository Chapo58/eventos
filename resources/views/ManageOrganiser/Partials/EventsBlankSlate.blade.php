@extends('Shared.Layouts.BlankSlate')

@section('blankslate-icon-class')
    ico-ticket
@stop

@section('blankslate-title')
    ¡Aún no hay eventos!
@stop

@section('blankslate-text')
    Parece que todavía tienes que crear un evento. Puedes crear uno haciendo clic en el botón de abajo.
@stop

@section('blankslate-body')
<button data-invoke="modal" data-modal-id="CreateEvent" data-href="{{route('showCreateEvent', ['organiser_id' => $organiser->id])}}" href='javascript:void(0);'  class="btn btn-success mt5 btn-lg" type="button">
    <i class="ico-ticket"></i>
    Crear Evento
</button>
@stop
