@extends('Shared.Layouts.MasterWithoutMenus')

@section('title')
Restablecer Contraseña
@stop

@section('content')
    <div class="row">
        <div class="col-md-4 col-md-offset-4">

           {!! Form::open(array('url' => route('postResetPassword'), 'class' => 'panel')) !!}

            <div class="panel-body">
                <div class="logo">
                   {!!HTML::image('assets/images/logo-dark.png')!!}
                </div>
                <h2>Restablecer Contraseña</h2>
                @if (Session::has('status'))
                <div class="alert alert-info">
                    Se ha enviado un correo electrónico con el restablecimiento de la contraseña.
                </div>
                @else

                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>¡Ups!</strong> Hubo algunos problemas con su entrada.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <div class="form-group">
                    {!! Form::label('email', 'Tu Email', ['class' => 'control-label']) !!}
                    {!! Form::text('email', null, ['class' => 'form-control', 'autofocus' => true]) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('password', 'Nueva Contraseña', ['class' => 'control-label']) !!}
                    {!! Form::password('password',  ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('password_confirmation', 'Confirmar Contraseña', ['class' => 'control-label']) !!}
                    {!! Form::password('password_confirmation',  ['class' => 'form-control']) !!}
                </div>
                {!! Form::hidden('token',  $token) !!}
                <div class="form-group nm">
                    <button type="submit" class="btn btn-block btn-success">Enviar</button>
                </div>
                <div class="signup">
                  <a class="semibold" href="{{route('login')}}">
                      <i class="ico ico-arrow-left"></i> Volver al login
                  </a>
                </div>
            </div>
            {!! Form::close() !!}

            @endif
        </div>
    </div>
@stop