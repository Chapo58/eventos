<?php

use Illuminate\Database\Seeder;

class TicketStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ticket_statuses = [
            [
                'id' => 1,
                'name' => 'Agotado',
            ],
            [
                'id' => 2,
                'name' => 'Las ventas han terminado',
            ],
            [
                'id' => 3,
                'name' => 'No está en venta todavía',
            ],
            [
                'id' => 4,
                'name' => 'En Venta',
            ],
            [
                'id' => 5,
                'name' => 'En Venta',
            ],
        ];

        DB::table('ticket_statuses')->insert($ticket_statuses);
    }
}
